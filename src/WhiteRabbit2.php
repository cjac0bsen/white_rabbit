<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */

    /**
     * @var array Coins denominations in descending order
     */
    public const COINS = array(100, 50, 20, 10, 5, 2, 1);

    public function findCashPayment(int $amount): array
    {
        $buffer = [];
        foreach (self::COINS as $denomination) {
            $buffer[$denomination] = floor($amount / $denomination);
            $amount -= $denomination * $buffer[$denomination];
        }
        return $buffer;
    }
}
