<?php

class WhiteRabbit
{
    public function findMedianLetterInFile(string $filePath): array
    {
        return array(
            'letter' => $this->findMedianLetter($this->parseFile($filePath), $occurrences),
            'count' => $occurrences
        );
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile(string $filePath): string
    {
        return mb_strtolower(str_replace(["\n", "\r", ' '], ['', '', ''], file_get_contents($filePath)));
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter(string $parsedFile, ?int &$occurrences): string
    {
        $alphabet = [
            'a' => 'a', 'b' => 'b', 'c' => 'c', 'd' => 'd', 'e' => 'e',
            'f' => 'f', 'g' => 'g', 'h' => 'h', 'i' => 'i', 'j' => 'j',
            'k' => 'k', 'l' => 'l', 'm' => 'm', 'n' => 'n', 'o' => 'o',
            'p' => 'p', 'q' => 'q', 'r' => 'r', 's' => 's', 't' => 't',
            'u' => 'u', 'v' => 'v', 'w' => 'w', 'x' => 'x', 'y' => 'y',
            'z' => 'z',
 
            'á' => 'á', 'é' => 'é', 'í' => 'í', 'ó' => 'ó', 'ú' => 'ú',
            'ä' => 'ä', 'ë' => 'ë', 'ï' => 'ï', 'ö' => 'ö', 'ü' => 'ü',
            'à' => 'à', 'è' => 'è', 'ì' => 'ì', 'ò' => 'ò', 'ù' => 'ù',
            'â' => 'â', 'ê' => 'ê', 'î' => 'î', 'ô' => 'ô', 'û' => 'û',
            'ã' => 'ã', 'õ' => 'õ', 'ñ' => 'ñ',
            'å' => 'å', 'æ' => 'æ', 'ø' => 'ø',
            '{' => '{', '}' => '}', '£' => '£', '=' => '=',
        ];

        $histogram = [];
        foreach ($alphabet as $letter) {
            if ($count = mb_substr_count($parsedFile, $letter)) {
                $histogram[$letter] = $count;
            }
        }

        arsort($histogram);
        $keys = array_keys($histogram);
        $pos = ceil(count($histogram) / 2) - 1;
        $occurrences = $histogram[$keys[$pos]];
        return $keys[$pos];
    }
}
